package com.mobile.ict.cart.container;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 3/5/16.
 */
public class Orders implements ParentListItem {



    OrderDetails orderDetails;
     List<ArrayList<String[]>> items;

    public Orders(OrderDetails orderDetails, List<ArrayList<String[]>> items) {
        this.orderDetails = orderDetails;
        this.items = items;
    }

    public OrderDetails getOrderDetails() {
        return orderDetails;
    }

    @Override
    public List<?> getChildItemList() {
        return items;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
