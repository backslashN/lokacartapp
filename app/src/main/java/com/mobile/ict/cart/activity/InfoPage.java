package com.mobile.ict.cart.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mobile.ict.cart.R;
import com.mobile.ict.cart.fragment.AboutUsFragment;
import com.mobile.ict.cart.fragment.FAQFragment;
import com.mobile.ict.cart.fragment.ProcessedOrderFragment;
import com.mobile.ict.cart.fragment.TermsAndConditionsFragment;
import com.mobile.ict.cart.util.Master;

public class InfoPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_page);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String open = getIntent().getStringExtra("open");
        if(open.equals("faq")){
            getSupportFragmentManager().beginTransaction().replace(R.id.info_container, new FAQFragment(), Master.FAQ_TAG).commit();

        }else if(open.equals("tnc")){
            getSupportFragmentManager().beginTransaction().replace(R.id.info_container, new TermsAndConditionsFragment(), Master.TERMS_AND_CONDITIONS_TAG).commit();

        }else{
            getSupportFragmentManager().beginTransaction().replace(R.id.info_container, new AboutUsFragment(), Master.ABOUT_US_TAG).commit();

        }


    }
}
